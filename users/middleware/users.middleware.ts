import express from 'express';
import userService from '../services/users.service';

class UsersMiddleware {
    async validateSameEmailDoesntExist(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        const user = await userService.getUserByEmail(req.body.email);
        if (user) {
            res.status(400).send({ errors: ['User with this email already exists'] });
        } else {
            next();
        }
    }

    async validateInvitee(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        if(req.body.email){
            const user = await userService.getUserByEmail(req.body.email);
            if (user) {
                res.status(400).send({ errors: ['User with this email already exists'] });
            } else {
                next();
            }
        } else if (req.body.phoneNumber) {
            const user = await userService.getUserByPhoneNumber(req.body.phoneNumber);
            if (user) {
                res.status(400).send({ errors: ['User with this phoneNumber already exists'] });
            } else {
                next();
            }
        } else {
            res.status(400).send('Invalid input')
        }

    }

    async validateSameEmailBelongToSameUser(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        if (res.locals.user._id === req.params.userId) {
            next();
        } else {
            res.status(400).send({ errors: ['Invalid email'] });
        }
    }

    async userCantChangePermission(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        if (
            'permissionFlags' in req.body &&
            req.body.permissionFlags !== res.locals.user.permissionFlags
        ) {
            res.status(400).send({
                errors: ['User cannot change permission flags'],
            });
        } else {
            next();
        }
    }

    validatePatchEmail = async (
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) => {
        if (req.body.email) {
            this.validateSameEmailBelongToSameUser(req, res, next);
        } else {
            next();
        }
    };

    async validateUserExists(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        const user = await userService.readById(req.params.userId);
        res.send({user})
        if (user) {
            res.locals.user = user;
            next();
        } else {
            res.status(404).send({
                errors: [`User ${req.params.userId} not found`],
            });
        }
    }

    async extractUserId(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        req.body.id = req.params.userId;
        next();
    }
}

export default new UsersMiddleware();