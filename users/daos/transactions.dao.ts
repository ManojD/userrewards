import mongooseService from '../../common/services/mongoose.service';
import shortid from 'shortid';
import debug from 'debug';
// import { UsersDao } from '../daos/users.dao'
import { CreateTransactionDto } from '../dto/create.transaction.dto';
import { PermissionFlag } from '../../common/middleware/common.permissionflag.enum';
import { Mongoose } from 'mongoose';

const log: debug.IDebugger = debug('app:users-dao');

class TransactionDao {
    Schema = mongooseService.getMongoose().Schema;

    transactionSchema = new this.Schema({
        // userId: { type: this.Schema.Types.ObjectId, required: true, ref: "User"},
        userId: String,
        _id: String,
        transactionType: String,
        purpose: String,
        amount: Number,
        accountNumber: Number,
        accountHolderName: String,
        ifscCode: String,
        bankName: String,
        permissionFlags: Number,
    }, { id: false });

    Transaction = mongooseService.getMongoose().model('Transaction', this.transactionSchema);

    constructor() {
        log('Created new instance of TransactionDao');
    }


    async addTransaction(userFields: CreateTransactionDto) {
        const userDetails = JSON.parse(JSON.stringify(userFields).split('"id":').join('"userId":'));
        const transactionId = shortid.generate();
        const user = new this.Transaction({
            _id: transactionId,
            ...userDetails,
            permissionFlags: PermissionFlag.FREE_PERMISSION,
        });
        await user.save();
        return transactionId;
    }
}

export default new TransactionDao();