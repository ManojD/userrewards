import mongooseService from '../../common/services/mongoose.service';
import shortid from 'shortid';
import debug from 'debug';
import { CreateUserDto } from '../dto/create.user.dto';
import { PatchUserDto } from '../dto/patch.user.dto';
import { PutUserDto } from '../dto/put.user.dto';
import { PermissionFlag } from '../../common/middleware/common.permissionflag.enum';

const log: debug.IDebugger = debug('app:users-dao');

class UsersDao {
    Schema = mongooseService.getMongoose().Schema;

    userSchema = new this.Schema({
        _id: String,
        email: String,
        password: { type: String, select: false },
        firstName: String,
        lastName: String,
        phoneNumber: Number,
        referrenceCode: String,
        referredCode: String,
        rewards: { type:Number, default: 0},
        invitationCount: { type:Number, default: 0},
        transactionByInvitee: {
            type: Boolean,
            default: false
        },
        firstTransaction: { type: Boolean, default: false},
        permissionFlags: Number,
    }, { id: false });

    User = mongooseService.getMongoose().model('Users', this.userSchema);

    constructor() {
        log('Created new instance of UsersDao');
    }

    generateReferrenceCode() {
          
        var string = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        let referrenceCode = '';
          
        var len = string.length;
        for (let i = 0; i < 6; i++ ) {
            referrenceCode += string[Math.floor(Math.random() * len)];
        }
        return referrenceCode;
    }

    async addUser(userFields: CreateUserDto) {
        const userId = shortid.generate();
        const referrence = this.generateReferrenceCode();
        // return referrenceCode;
        const user = new this.User({
            _id: userId,
            ...userFields,
            referrenceCode: referrence,
            permissionFlags: PermissionFlag.FREE_PERMISSION,
        });
        await user.save();
        return userId;
    }

    async getUserByEmail(email: string) {
        return this.User.findOne({ email: email }).exec();
    }

    async getUserByEmailWithPassword(email: string) {
        return this.User.findOne({ email: email })
            .select('_id email permissionFlags +password')
            .exec();
    }

    async removeUserById(userId: string) {
        return this.User.deleteOne({ _id: userId }).exec();
    }

    async getUserByReferredCode(code: string) {
        return this.User.findOne({ referrenceCode: code }).populate('User').exec();
    }

    async getUserById(userId: string) {
        return this.User.findOne({ _id: userId }).populate('User').exec();
    }

    async getUserByPhoneNumber(phoneNumber: number) {
        return this.User.findOne({ phoneNumber: phoneNumber }).populate('User').exec();
    }

    async getUsers(limit = 25, page = 0) {
        return this.User.find()
            .limit(limit)
            .skip(limit * page)
            .exec();
    }

    async updateUserById(
        userId: string,
        userFields: PatchUserDto | PutUserDto
    ) {
        const existingUser = await this.User.findOneAndUpdate(
            { _id: userId },
            { $set: userFields },
            { new: true }
        ).exec();

        return existingUser;
        // return userId
    }
}

export default new UsersDao();