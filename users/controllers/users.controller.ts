import express from 'express';
import usersService from '../services/users.service';
import argon2 from 'argon2';
import debug from 'debug';
import { PatchUserDto } from '../dto/patch.user.dto';

const log: debug.IDebugger = debug('app:users-controller');

class UsersController {
    async listUsers(req: express.Request, res: express.Response) {
        res.send(req.body)
        const users = await usersService.list(100, 0);
        res.status(200).send(users);
    }

    async getUserById(req: express.Request, res: express.Response) {
        const user = await usersService.readById(req.body.id);
        res.status(200).send(user);
    }

    async sendInvite(req: express.Request, res: express.Response) {
        const user = await usersService.readById(req.body.id);
        if(user) {
            user.rewards += 1;
            user.invitationCount +=1;
            log( await usersService.patchByReferredUserId(user._id, user));
            res.status(200).send(user);
        }
    }

    

    async createUser(req: express.Request, res: express.Response) {
        if(!req.body.referredCode) {
            req.body.password = await argon2.hash(req.body.password);
            const userId = await usersService.create(req.body);
            res.status(201).send({ id: userId });
        } else {
            req.body.password = await argon2.hash(req.body.password);
            const referredUser = await usersService.readByReferredCode(req.body.referredCode)
            if(referredUser){
                const userId = await usersService.create(req.body);
                referredUser.rewards += 2;
                log( await usersService.patchByReferredUserId(referredUser._id, referredUser));
                res.status(201).send({ id: userId})

            }else {
                res.status(400).send("invalid referral code")
            }
        }
    }

    async transaction(req: express.Request, res: express.Response){
        const user = await usersService.readById(req.body.id);
        if(!user.firstTransaction && user.referredCode){
            const referredUsers = await usersService.readByReferredCode(user.referredCode)
            referredUsers.rewards += 5;
            user.firstTransaction = true;
            log( await usersService.patchByReferredUserId(referredUsers._id, referredUsers));
            log( await usersService.patchByReferredUserId(user._id, user));
            const transactionId = await usersService.createTransaction(req.body);
            res.status(201).send({ id: transactionId})
        }
        else {
            const transactionId = await usersService.createTransaction(req.body);
            res.status(201).send({ id: transactionId})
        }
    }

    async patch(req: express.Request, res: express.Response) {
        if (req.body.password) {
            req.body.password = await argon2.hash(req.body.password);
        }
        log(await usersService.patchById(req.body.id, req.body));
        res.status(204).send();
    }

    async put(req: express.Request, res: express.Response) {
        req.body.password = await argon2.hash(req.body.password);
        log(await usersService.putById(req.body.id, req.body));
        res.status(204).send();
    }

    async removeUser(req: express.Request, res: express.Response) {
        log(await usersService.deleteById(req.body.id));
        res.status(204).send();
    }

    async updatePermissionFlags(req: express.Request, res: express.Response) {
        const patchUserDto: PatchUserDto = {
            permissionFlags: parseInt(req.params.permissionFlags),
        };
        log(await usersService.patchById(req.body.id, patchUserDto));
        res.status(204).send();
    }
}

export default new UsersController();
