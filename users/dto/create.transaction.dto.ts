export interface CreateTransactionDto {
    userId: string;
    transactionType: string;
    purpose?: string;
    amount: number;
    accountNumber: number,
    accountHolderName: string,
    ifscCode: string,
    bankName: string,
    permissionFlags: number,
    
}