export interface PutUserDto {
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    phoneNumber: number,
    referrenceCode: string,
    referredCode: string,
    rewards: number,
    invitationCount: number,
    firstTransaction: boolean,
    transactionByInvitee: boolean,
    permissionFlags: number;
}