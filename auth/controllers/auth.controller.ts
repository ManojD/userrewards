import dotenv from 'dotenv';
const dotenvResult = dotenv.config();
if (dotenvResult.error) {
    throw dotenvResult.error;
}

import express from 'express';
import debug from 'debug';
import jwt from 'jsonwebtoken';
import crypto from 'crypto';

const log: debug.IDebugger = debug('app:auth-controller');

// @ts-expect-error
const jwtSecret: string = process.env.JWT_SECRET;
const tokenExpirationInSeconds = 36000;

class AuthController {
    async createJWT(req: express.Request, res: express.Response) {
        try {
            // res.send(req.body)
            // const refreshId = req.body.userId + jwtSecret;
            // const buff = crypto.randomBytes(16)
            // // const salt = crypto.createSecretKey(crypto.randomBytes(16));
            // const salt = crypto.createSecretKey(buff);
            // // const salt = crypto.createPrivateKey(buff.toString());
            // res.send(salt)
            // const hash = crypto
            //     .createHmac('sha512', salt)
            //     .update(refreshId)
            //     .digest('base64');
            // req.body.refreshKey = salt.export();
            const token = jwt.sign(req.body, jwtSecret, {
                expiresIn: tokenExpirationInSeconds,
            });
            return res
                .status(201)
                .send({ accessToken: token});
        } catch (err) {
            log('createJWT error: %O', err);
            // return res.status(500).send();
        }
    }
}

export default new AuthController();