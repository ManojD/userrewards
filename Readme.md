# Backend
The backend repository which contian all services in single project. 

## Prerequisites
1. Install [PM2](http://pm2.keymetrics.io/) globally. `npm install pm2 -g`
2. Node version required: `^12.0.0`

## Dependencies
* argon2
* cors
* crypto
* debug
* dotenv
* express
* express-validator
* express-winston
* helmet
* jsonwebtoken
* mongoose
* shortid
* winston
* chai
* mocha
* source-map-support
* supertest
* ts-node
* typescript

## Getting Started
1. Clone this repository.
2. Run `npm install`
3. Create your feature branch and start working on it.
4. Add `.env` file.
5. To start Development environment use `tsc && node --unhandled-rejections=strict ./dist/app.js`

Update the .env details locally
<h2>.env.sample</h2>

PORT = ""
JWT_SECRET=""

## Application Structure

- `app.ts` - The entry point to our application. This file defines our express server and connects it to MongoDB using mongoose. It also requires the routes we'll be using in the application.
- `auth/` - This folder contains the authentications for the application API.
- `common/` - This folder contains all the common definitions for everything in the app.
- `users/` - This folder contains all the crud calls and business logic which are linked by API .
- `test/` - This folder contains all the test cases for testing the applcaition API.